require('colors');

const dbus = require('dbus');
const fs = require('fs');
const request = require('request');

const SERVICE_NAME = 'org.mpris.MediaPlayer2.spotify';
const OBJ_PATH = '/org/mpris/MediaPlayer2';

const INTERFACE_PLAYER = 'org.mpris.MediaPlayer2.Player';
const INTERFACE_PROPERTIES = 'org.freedesktop.DBus.Properties';
const REQUEST_OPTIONS = {
    encoding: null
};

let previousTrackId = null;
const bus = dbus.getBus('session');

function writeTrackInfo(metadata) {
    const trackInfo = {
        artist: metadata['xesam:artist'][0],
        title: metadata['xesam:title']
    };

    log(trackInfo.artist + ' - ' + trackInfo.title);

    const onDataWritten = function (err) {
        if (err) {
            console.error(err);
        }
    };

    fs.writeFile(__dirname + '/track_title.txt', trackInfo.title, onDataWritten);
    fs.writeFile(__dirname + '/track_artist.txt', trackInfo.artist, onDataWritten);
}

function downloadArtwork(metadata) {
    if (!metadata['mpris:artUrl']) {
        fs.copyFile(__dirname + '/artwork_default.jpg', __dirname + '/artwork.jpg', function onFileCopied(err) {
            if (err) {
                console.error(err);
            }

            return;
        });
    }
    else {
        request.get(metadata['mpris:artUrl'], REQUEST_OPTIONS, function (err, res, body) {
            if (err) {
                console.error(err);
                return;
            }

            fs.writeFile(__dirname + '/artwork.jpg', body, function onImageWritten(err) {
                if (err) {
                    console.error(err);
                    return;
                }

                log(metadata['mpris:artUrl']);
            });
        });
    }
}

bus.getInterface(SERVICE_NAME, OBJ_PATH, INTERFACE_PLAYER, function onInterface(err, iface) {
    if (err) {
        console.error(err);
        return;
    }

    iface.getProperty('Metadata', function onMetadata(err, metadata) {
        previousTrackId = metadata['mpris:trackid'];

        writeTrackInfo(metadata);
        downloadArtwork(metadata);
    });
})


bus.getInterface(SERVICE_NAME, OBJ_PATH, INTERFACE_PROPERTIES, function onInterface(err, iface) {
    if (err) {
        console.error(err);
        return;
    }

    iface.on('PropertiesChanged', function onPropertiesChanged(ifaceName, changedProps) {
        if (ifaceName != INTERFACE_PLAYER || !changedProps.Metadata || previousTrackId === changedProps.Metadata['mpris:trackid']) {
            return;
        }

        previousTrackId = changedProps.Metadata['mpris:trackid'];

        log('');
        writeTrackInfo(changedProps.Metadata);
        downloadArtwork(changedProps.Metadata);
    });
});

function log(msg) {
    const date = (new Date()).toLocaleString();

    console.info(('[%s]: '.gray) + msg, date.toLocaleString());
};